# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\adt\android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontwarn java.nio.**
-keep class java.nio.** { *; }
-dontwarn okio.**
-keep class okio.** { *; }
-dontwarn okhttp.**
-keep class okhttp.** { *; }
-dontwarn okhttp2.**
-keep class okhttp2.** { *; }
-dontwarn okhttp3.**
-keep class okhttp3.** { *; }
-dontwarn retrofit.**
-keep class retrofit.** { *; }
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}