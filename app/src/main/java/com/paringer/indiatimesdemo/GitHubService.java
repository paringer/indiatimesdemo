package com.paringer.indiatimesdemo;

import java.io.Serializable;
import java.util.List;

import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.Call;

/**
 * git hub repo service, just for debug, of retrofit new version
 */
public interface GitHubService {
    /**
     * git hub repo, just for debug, of retrofit new version
     */
    public static class Repo implements Serializable {
        public String name;
        public String url;

        public Repo(String name, String url) {
            this.name = name;
            this.url = url;
        }
    }

//    @GET("/users/{user}/repos")
//    void listRepos(@Path("user") String user);

//    @GET("/users/{user}/repos")
//    void listRepos(@Path("user") String user, Callback<List<Repo>> callback);

  @GET("/users/{user}/repos")
  Call<List<Repo> > listRepos(@Path("user") String user, Callback<List<Repo> > wtf);
  @GET("/users/{user}/repos")
  Call<List<Repo> > listRepos(@Path("user") String user);
}