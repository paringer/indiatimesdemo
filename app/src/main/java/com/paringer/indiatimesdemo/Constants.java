package com.paringer.indiatimesdemo;

import java.net.URL;

/**
 * Created by Zhenya on 04.01.2016.
 */
public interface Constants {
    public static final String SITE_URL = "http://timesofindia.indiatimes.com";
    public static final String GIT_HUB_URL = "https://api.github.com";

}
