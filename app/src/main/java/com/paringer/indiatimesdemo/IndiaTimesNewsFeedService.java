package com.paringer.indiatimesdemo;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.util.Collection;
import java.util.List;
import java.util.Set;

//import retrofit.Callback;
//import retrofit.http.GET;
//import retrofit.http.Headers;
//import retrofit.http.Query;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * india times service
 */
public interface IndiaTimesNewsFeedService {
    /**
     * india times news page
     */
    public static class NewsPage {
        /**
         * page information
         */
        public static class Pagination {
            public int TotalPages;
            public int PageNo;
            public int PerPage;
            public String WebURL;
        }

        @SerializedName("Pagination")
        public Pagination Pagination;

        /**
         * news item
         */
        public static class NewsItem {
            public int NewsItemId;
            public String HeadLine;
            public String Agency;
            public String ByLine;
            public String DateLine;
            public String WebURL;
            public String Caption;

            /**
             * news image
             */
            public static class Image {
                public String Photo;
                public String Thumb;
                public String PhotoCaption;
            }

            @SerializedName("Image")
            public Image Image;

            /**
             * news video
             */
            public static class Video {
                public String Thumb;
                public String VideoCaption;
                public String DetailFeed;
                public String Type;
            }

            @SerializedName("Video")
//            public List<Video> VideoList;//bad field
//            public Video VideoSingle;//bad field
            public com.google.gson.JsonElement Video;//good field
            public String Keywords;
            public String Story;

            /**
             * related news
             */
            public static class HomeRelated {
                public int NewsItemId;
                public String HeadLine;
                public String DetailFeed;
                public String Type;
            }

            @SerializedName("HomeRelated")
            public JsonElement HomeRelated;
            public String CommentCountUrl;
            public String CommentFeedUrl;
            public String Related;
        }

        @SerializedName("NewsItem")
        public List<NewsItem> NewsItem;
    }

    @GET("/feeds/newsdefaultfeeds.cms")
    Call<NewsPage> feedNews(@Query("feedtype") String feedType);

}
