package com.paringer.indiatimesdemo.dao;

/**
 * Created with IntelliJ IDEA.
 * Where: Home
 * Author: e.paringer
 * Date: 2015-01-19
 * Time: 15:19
 * To change this template use File | Settings | File Templates.
 */

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.paringer.indiatimesdemo.R;

import static com.paringer.indiatimesdemo.dao.TopicsDAO.*;

/**
 * A custom Content Provider to do the database operations
 */
public class NewsProvider extends ContentProvider {

    public static final String PROVIDER_NAME = "com.paringer.indiatimesdemo";

    /**
     * A uri to do operations on news_topics table. A content provider is identified by its uri
     */
    public static final Uri CONTENT_URI = Uri.parse("content://" + PROVIDER_NAME + "/topics");

    /**
     * Constants to identify the requested operation
     */
    private static final int TOPICS = 1;

    private static final UriMatcher uriMatcher;
    public static final String CURRENT_FEED_SHORT_NAME = "http://timesofindia.indiatimes.com/feeds/newsdefaultfeeds.cms?feedtype=sjson";

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "topics", TOPICS);
    }

    /**
     * This content provider does the database operations by this object
     */
    TopicsDAO mTopicsDAO;
    protected String mCurrentRssName;

    /**
     * A callback method which is invoked when the content provider is starting up
     */
    @Override
    public boolean onCreate() {
        mTopicsDAO = new TopicsDAO(getContext());
        return true;
    }

    /**
     * gets type
     *
     * @param uri
     * @return
     */
    @Override
    public String getType(Uri uri) {
        return null;
    }

    /**
     * A callback method which is by the default content uri,
     * CRUD operation
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        if (uriMatcher.match(uri) == TOPICS) {
            mCurrentRssName = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(CURRENT_FEED_SHORT_NAME, getContext().getString(R.string.default_json_feed_short_name));
            return mTopicsDAO.getAllTopics(mCurrentRssName.hashCode());
        } else {
            return null;
        }
    }

    /**
     * A callback method which is by the default content uri,
     * CRUD operation
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        mTopicsDAO.getWritableDatabase().delete(DB_TABLE, selection, selectionArgs);
        return 0;
    }

    /**
     * A callback method which is by the default content uri,
     * CRUD operation
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        mTopicsDAO.getWritableDatabase().insertWithOnConflict(DB_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        return null;
    }

    /**
     * A callback method which is by the default content uri,
     * CRUD operation
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        mTopicsDAO.getWritableDatabase().updateWithOnConflict(DB_TABLE, values, selection, selectionArgs, SQLiteDatabase.CONFLICT_REPLACE);
        return 0;
    }
}
