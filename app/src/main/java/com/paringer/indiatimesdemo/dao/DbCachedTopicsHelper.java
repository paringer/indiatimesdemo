package com.paringer.indiatimesdemo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.paringer.indiatimesdemo.IndiaTimesNewsFeedService;
import com.paringer.indiatimesdemo.IndiaTimesNewsFeedService.NewsPage;
import com.paringer.indiatimesdemo.IndiaTimesNewsFeedService.NewsPage.NewsItem;
import com.paringer.indiatimesdemo.dao.TopicsDAO;

import java.util.ArrayList;
import java.util.List;

import static com.paringer.indiatimesdemo.dao.TopicsDAO.*;

/**
 * Created with IntelliJ IDEA.
 * Where: Home
 * Author: e.paringer
 * Date: 2015-01-20
 * Time: 17:37
 * To change this template use File | Settings | File Templates.
 */
public class DbCachedTopicsHelper {
    private final TopicsDAO mDB;

    /**
     * constructor
     *
     * @param context
     */
    public DbCachedTopicsHelper(Context context){
        mDB = new TopicsDAO(context);
    }

    /**
     * constructor
     *
     * @param topicsDAO
     */
    public DbCachedTopicsHelper(TopicsDAO topicsDAO){
        mDB = topicsDAO;
    }

    /**
     * inserts news
     * @param feed
     * @param siteNumber
     */
    public void insertFeed(NewsPage feed, Integer siteNumber) {
//        SQLiteDatabase r = mDB.getReadableDatabase();
        SQLiteDatabase w = mDB.getWritableDatabase();
        List<NewsItem> rssItems = feed.NewsItem;
        for(NewsItem atom : rssItems){
//            Cursor c;
//            if(siteNumber != null) {c = r.query(DB_TABLE, new String[]{TOPIC_GUID, TOPIC_SITE}, TOPIC_GUID+" = ? AND "+TOPIC_SITE+" = ? ", new String[]{atom.getGuid(), siteNumber.toString()},null,null,null);}
//            else {c = r.query(DB_TABLE, new String[]{TOPIC_GUID}, TOPIC_GUID+" = ? ", new String[]{atom.getGuid()},null,null,null);}
//            if(c.getCount()==0)
            {
                ContentValues cv = new ContentValues();
                cv.put(TOPIC_TITLE, atom.HeadLine);
                cv.put(TOPIC_LINK, atom.WebURL);
                cv.put(TOPIC_AUTHOR, atom.ByLine);
                cv.put(TOPIC_AGENCY, atom.Agency);
                cv.put(TOPIC_PUBLICATION_DATE, atom.DateLine);
                cv.put(TOPIC_CATEGORY_DOMAIN, atom.Keywords);
                cv.put(TOPIC_CATEGORY, atom.Keywords);
                cv.put(TOPIC_GUID, atom.NewsItemId);
                cv.put(TOPIC_DESCRIPTION, atom.Caption);
                cv.put(TOPIC_DESCRIPTION_THUMB, atom.Image==null?null:atom.Image.Thumb);
                cv.put(TOPIC_DESCRIPTION_TEXT, atom.Image==null?null:atom.Image.PhotoCaption);
                cv.put(TOPIC_DESCRIPTION_IMG, atom.Image==null?null:atom.Image.Photo);
                cv.put(TOPIC_COMMENTS, atom.CommentFeedUrl);
                cv.put(TOPIC_CONTENT, atom.Story);
                cv.put(TOPIC_MEDIA_CONTENT, atom.Video==null?null:atom.Video.toString());
                cv.put(TOPIC_SITE, siteNumber);
                w.insertWithOnConflict(DB_TABLE,null,cv,SQLiteDatabase.CONFLICT_REPLACE);
            }
//            c.close();
        }
        w.close();
//        r.close();
    }

    /**
     * removes all news
     */
    public void clearTable() {
        SQLiteDatabase w = mDB.getWritableDatabase();
        w.delete(DB_TABLE,null,null);
        w.close();
    }
}
