package com.paringer.indiatimesdemo.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created with IntelliJ IDEA.
 * Where: Home
 * Author: e.paringer
 * Date: 2015-01-20
 * Time: 00:16
 * To change this template use File | Settings | File Templates.
 */
public class ContentCacheDAO extends SQLiteOpenHelper {

    /**
     * Database name
     */
    public static final String DBNAME = "india_times_db";
    /**
     * Version number of the database
     */
    private static int VERSION = 1;
    /** An instance variable for SQLiteDatabase */
//    private SQLiteDatabase mDB;

    /**
     * A constant, stores the the table name
     */
    public static final String DB_TABLE = "news_content_cache";

    /**
     * Field 1 of the table news_topics, which is the primary key
     */
    public static final String KEY_ROW_ID = "_id";
    public static final String CONTENT_URL = "url";
    //    public static final String CONTENT_GUID = "guid";
    public static final String CONTENT_DATA = "data";

    /**
     * Constructor
     */
    public ContentCacheDAO(Context context) {
        super(context, DBNAME, null, VERSION);
        SQLiteDatabase db = getWritableDatabase();
        db.close();
    }

    /**
     * This is a callback method, invoked when the method
     * getReadableDatabase() / getWritableDatabase() is called
     * provided the database does not exists
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table " + DB_TABLE + " ( "
                + KEY_ROW_ID + " integer primary key autoincrement , "
                + CONTENT_URL + " text ,"
//                + CONTENT_GUID                  + " text ,"
                + CONTENT_DATA + " blob )";

        db.execSQL(sql);

        sql = "CREATE INDEX " + DB_TABLE + "_" + CONTENT_URL + "_idx ON " + DB_TABLE + "(" + CONTENT_URL + ");";
        db.execSQL(sql);

//        insertMockData(db);
    }

    /**
     * inserdts mock data
     *
     * @param db
     */
    public void insertMockData(SQLiteDatabase db) {

        String sql = "insert into " + DB_TABLE +
                " ( " + CONTENT_URL +
//                "," + CONTENT_GUID +
                "," + CONTENT_DATA +
                " ) "
                + " values ( " +
                "'http://podrobnosti.ua/society/2015/01/18/1011771.html' , " +
//                "'p1011771' , " +
                "'<img src=\"http://podrobnosti.ua/upload/news/2015/01/18/1011771_3.jpg\" alt=\"\" width=\"58\" height=\"44\" /> Украина вернет Донбасс и возродит там все украинское. Об этом в своем выступлении на Марше памяти жертв террора под Волновахой заявил президент Петр Порошенко. Украина продемонстрировала всему миру, что является миролюбивым европейским государством, отметил президент.' ) ";
        db.execSQL(sql);

    }

    /**
     * Returns all the customers in the table
     */
    public Cursor getAllContent() {
        SQLiteDatabase db = getReadableDatabase();//do not close it until cursor released
        return db.query(DB_TABLE, new String[]{KEY_ROW_ID, CONTENT_URL,
//                        CONTENT_GUID,
                        CONTENT_DATA},
                null, null, null, null,
                KEY_ROW_ID + " asc ");
    }

    /**
     * db version upgrade listener
     *
     * @param arg0
     * @param arg1
     * @param arg2
     */
    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        // my code here
    }
}
