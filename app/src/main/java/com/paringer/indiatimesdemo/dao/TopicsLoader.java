package com.paringer.indiatimesdemo.dao;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.SimpleCursorAdapter;

import java.lang.ref.WeakReference;

/**
 * Topics Loader
 */
public class TopicsLoader implements LoaderManager.LoaderCallbacks<Cursor> {
    private WeakReference<SimpleCursorAdapter> mAdapter;
    private WeakReference<Context> mContext;

    /**
     * constructor
     *
     * @param context
     * @param adapter list adapter
     */
    public TopicsLoader(Context context, SimpleCursorAdapter adapter) {
        this.mContext = new WeakReference<>(context);
        this.mAdapter = new WeakReference<>(adapter);
    }

    /**
     * A callback method invoked by the loader when initLoader() is called
     */
    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
        Uri uri = NewsProvider.CONTENT_URI;
        Context context = mContext.get();
        if (context != null) {
            return new CursorLoader(context, uri, null, null, null, null);
        } else {
            return null;
        }
    }

    /**
     * A callback method, invoked after the requested content provider returned all the data
     */
    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
        mAdapter.get().swapCursor(arg1);
    }

    /**
     * A callback method, invoked at loader destruction
     */
    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        mAdapter.get().swapCursor(null);
    }

}
