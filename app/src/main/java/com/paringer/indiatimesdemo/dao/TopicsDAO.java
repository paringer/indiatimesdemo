package com.paringer.indiatimesdemo.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * Where: Home
 * Author: e.paringer
 * Date: 2015-01-18
 * Time: 21:07
 * To change this template use File | Settings | File Templates.
 */
public class TopicsDAO extends SQLiteOpenHelper {

    /**
     * Database name
     */
    public static final String DBNAME = "india_times_db";
    /**
     * Version number of the database
     */
    private static int VERSION = 1;
    /** An instance variable for SQLiteDatabase */
//    private SQLiteDatabase mDB;

    /**
     * A constant, stores the the table name
     */
    public static final String DB_TABLE = "news_topics";

    /**
     * Field 1 of the table news_topics, which is the primary key
     */
    public static final String KEY_ROW_ID = "_id";
    public static final String TOPIC_TITLE = "title";
    public static final String TOPIC_LINK = "link";
    public static final String TOPIC_AUTHOR = "author";
    public static final String TOPIC_AGENCY = "agency";
    public static final String TOPIC_PUBLICATION_DATE = "pub_date";
    public static final String TOPIC_CATEGORY_DOMAIN = "category_domain";
    public static final String TOPIC_CATEGORY = "category";
    public static final String TOPIC_GUID = "guid";

    public static final String TOPIC_DESCRIPTION = "description";
    public static final String TOPIC_DESCRIPTION_THUMB = "description_thumb";
    public static final String TOPIC_DESCRIPTION_IMG = "description_img";
    public static final String TOPIC_DESCRIPTION_TEXT = "description_text";

    public static final String TOPIC_COMMENTS = "comments";
    public static final String TOPIC_CONTENT = "content";
    public static final String TOPIC_MEDIA_CONTENT = "media_content";

    public static final String TOPIC_SITE = "media_site";

    /**
     * Constructor
     */
    public TopicsDAO(Context context) {
        super(context, DBNAME, null, VERSION);
        SQLiteDatabase db = getWritableDatabase();
        db.close();
    }

    /**
     * This is a callback method, invoked when the method
     * getReadableDatabase() / getWritableDatabase() is called
     * provided the database does not exists
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table " + DB_TABLE + " ( "
                + KEY_ROW_ID + " integer primary key autoincrement , "
                + TOPIC_TITLE + " text ,"
                + TOPIC_LINK + " text ,"
                + TOPIC_AUTHOR + " text ,"
                + TOPIC_AGENCY + " text ,"
                + TOPIC_PUBLICATION_DATE + " text ,"
                + TOPIC_CATEGORY_DOMAIN + " text ,"
                + TOPIC_CATEGORY + " text ,"
                + TOPIC_GUID + " text ,"
                + TOPIC_DESCRIPTION + " text ,"
                + TOPIC_DESCRIPTION_THUMB + " text ,"
                + TOPIC_DESCRIPTION_IMG + " text ,"
                + TOPIC_DESCRIPTION_TEXT + " text ,"
                + TOPIC_CONTENT + " text ,"
                + TOPIC_COMMENTS + " text ,"
                + TOPIC_MEDIA_CONTENT + " text ,"
                + TOPIC_SITE + " integer ," +
                "UNIQUE(" +
                TOPIC_SITE +
                ", " +
                TOPIC_GUID +
                ") )";

        db.execSQL(sql);
        //now indexes
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_LINK + "_idx ON " + DB_TABLE + "(" + TOPIC_LINK + ");";
        db.execSQL(sql);
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_GUID + "_idx ON " + DB_TABLE + "(" + TOPIC_GUID + ");";
        db.execSQL(sql);
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_SITE + "_idx ON " + DB_TABLE + "(" + TOPIC_SITE + ");";
        db.execSQL(sql);
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_TITLE + "_idx ON " + DB_TABLE + "(" + TOPIC_TITLE + ");";
        db.execSQL(sql);
        sql = "CREATE INDEX " + DB_TABLE + "_" + TOPIC_PUBLICATION_DATE + "_idx ON " + DB_TABLE + "(" + TOPIC_PUBLICATION_DATE + ");";
        db.execSQL(sql);


        insertMockData(db);

    }

    /**
     * generates mock data
     *
     * @param db database
     */
    public void insertMockData(SQLiteDatabase db) {
        /*
    ,{	"NewsItemId":"50383422"
		,"HeadLine":"Air India flight halted after dog enters runway"
		,"ByLine":"Yudhvir Rana"
		,"Agency":"TNN"
		,"DateLine":"Dec 30, 2015, 07.34PM IST"
		,"WebURL":"http://timesofindia.indiatimes.com/city/amritsar/Dubai-bound-Air-India-flight-halted-after-dog-enters-runway/articleshow/50383422.cms"
		,"Caption":"Air India Express flight from Amritsar to Dubai was delayed for nearly four hours after a stray dog came on the runway while the flight was about to take off. The flight which was scheduled to leave Amritar for Dubai at 3.15pm could fly only at 7.10pm."
		,"Image":
		{"Photo":"http://timesofindia.indiatimes.com/photo.cms?photoid=50383452"
		,"Thumb":"http://timesofindia.indiatimes.com/thumb.cms?photoid=50383452"
		,"PhotoCaption":"(Representative image)"}
		,"Keywords":"runway, flight halted, Dog, Air India flight, Air India"
		,"Story":"AMRITSAR: Air India Express flight from Amritsar to Dubai was delayed for nearly four hours after a stray dog came on the runway while the flight was about to take off.\n\nAmritsar's Sri Guru Ram Das Ji International airport director Venkateshwar Rao informed on Wednesday that the aircraft was on runway to take off when the pilot noticed a stray dog and decided to abort the flight.\n\nThe flight which was scheduled to leave Amritar for Dubai at 3.15pm could fly only at 7.10pm.\n\nAccording to reports the pilot aborted the take off as a precautionary measure to check if there was any damage to the aircraft.\n\nWhen the aircraft came back from runway, it underwent inspection by technicians as well as by the pilot and took off after obtaining clearance from director general Civil Aviation."
		,"CommentCountUrl":"http://timesofindia.indiatimes.com/feeds/usercommentcount/50383422.cms"
		,"CommentFeedUrl":"http://timesofindia.indiatimes.com/feeds/usrcommentfeedsnew/50383422.cms?feedtype=sjson"
		,"Related":"http://timesofindia.indiatimes.com/newsrelatedfeeds/50383422.cms?feedtype=sjson"
	}
        */

        String sql = "insert into " + DB_TABLE + " ( " + TOPIC_SITE + "," + TOPIC_TITLE + "," + TOPIC_LINK + "," + TOPIC_AUTHOR + "," + TOPIC_AGENCY + "," + TOPIC_PUBLICATION_DATE + "," + TOPIC_CATEGORY_DOMAIN + "," + TOPIC_CATEGORY + "," + TOPIC_GUID + "," + TOPIC_DESCRIPTION + "," + TOPIC_DESCRIPTION_IMG + "," + TOPIC_DESCRIPTION_TEXT + "," + TOPIC_COMMENTS + "," + TOPIC_CONTENT + "," + TOPIC_MEDIA_CONTENT + " ) "
                + " values ( " +
                Integer.toString("http://timesofindia.indiatimes.com/feeds/newsdefaultfeeds.cms?feedtype=sjson".hashCode()) + " , " +
                "'Air India, flight halted, after dog enters runway' , " +
                "'http://timesofindia.indiatimes.com/city/amritsar/Dubai-bound-Air-India-flight-halted-after-dog-enters-runway/articleshow/50383422.cms' , " +
                "'Yudhvir Rana' , " +
                "'TNN' , " +
                "'Dec 30, 2015, 07.34PM IST' , " +
                "'http://podrobnosti.ua/society/' , " +
                "'runway, flight halted, Dog, Air India flight, Air India' , " +
                "'p1011771' , " +
                "'Air India Express flight from Amritsar to Dubai was delayed for nearly four hours after a stray dog came on the runway while the flight was about to take off. The flight which was scheduled to leave Amritar for Dubai at 3.15pm could fly only at 7.10pm.' , " +
                "'http://timesofindia.indiatimes.com/thumb.cms?photoid=50383452' , " +
                "'(Representative image)' , " +
                "'http://timesofindia.indiatimes.com/feeds/usrcommentfeedsnew/50383422.cms?feedtype=sjson' , " +
                "'AMRITSAR: Air India Express flight from Amritsar to Dubai was delayed for nearly four hours after a stray dog came on the runway while the flight was about to take off.\\n\\nAmritsar''s Sri Guru Ram Das Ji International airport director Venkateshwar Rao informed on Wednesday that the aircraft was on runway to take off when the pilot noticed a stray dog and decided to abort the flight.\\n\\nThe flight which was scheduled to leave Amritar for Dubai at 3.15pm could fly only at 7.10pm.\\n\\nAccording to reports the pilot aborted the take off as a precautionary measure to check if there was any damage to the aircraft.\\n\\nWhen the aircraft came back from runway, it underwent inspection by technicians as well as by the pilot and took off after obtaining clearance from director general Civil Aviation.' , " +
                "'http://timesofindia.indiatimes.com/thumb.cms?photoid=50383452' )";
        db.execSQL(sql);

        sql = "insert into " + DB_TABLE + " ( " + TOPIC_TITLE + "," + TOPIC_LINK + "," + TOPIC_PUBLICATION_DATE + "," + TOPIC_CATEGORY_DOMAIN + "," + TOPIC_CATEGORY + "," + TOPIC_GUID + "," + TOPIC_DESCRIPTION + "," + TOPIC_DESCRIPTION_IMG + "," + TOPIC_DESCRIPTION_TEXT + "," + TOPIC_CONTENT + "," + TOPIC_MEDIA_CONTENT + " ) "
                + " values ( " +
                "'mock news 2' , " +
                "'http://podrobnosti.ua/society/2015/01/18/1011771.html' , " +
                "'Sun, 17 Jan 2015 15:30:08 +0200' , " +
                "'http://podrobnosti.ua/society/' , " +
                "'Общество' , " +
                "'p1011772' , " +
                "'<img src=\"http://podrobnosti.ua/upload/news/2015/01/18/1011771_1.jpg\" alt=\"\" width=\"58\" height=\"44\" /> Украина вернет Донбасс и возродит там все украинское. Об этом в своем выступлении на Марше памяти жертв террора под Волновахой заявил президент Петр Порошенко. Украина продемонстрировала всему миру, что является миролюбивым европейским государством, отметил президент.' , " +
                "'http://podrobnosti.ua/upload/news/2015/01/18/1011771_1.jpg' , " +
                "'Украина вернет Луганск и возродит там все украинское. Об этом в своем выступлении на Марше памяти жертв террора под Волновахой заявил президент Петр Порошенко. Украина продемонстрировала всему миру, что является миролюбивым европейским государством, отметил президент.' , " +
                "'' , " +
                "'http://podrobnosti.ua/upload/news/2015/01/18/1011769_3.jpg' )";
        db.execSQL(sql);

        sql = "insert into " + DB_TABLE + " ( " + TOPIC_TITLE + "," + TOPIC_LINK + "," + TOPIC_PUBLICATION_DATE + "," + TOPIC_CATEGORY_DOMAIN + "," + TOPIC_CATEGORY + "," + TOPIC_GUID + "," + TOPIC_DESCRIPTION + "," + TOPIC_DESCRIPTION_IMG + "," + TOPIC_DESCRIPTION_TEXT + "," + TOPIC_CONTENT + "," + TOPIC_MEDIA_CONTENT + " ) "
                + " values ( " +
                "'mock news 3' , " +
                "'http://podrobnosti.ua/society/2015/01/18/1011771.html' , " +
                "'Sun, 16 Jan 2015 15:30:08 +0200' , " +
                "'http://podrobnosti.ua/society/' , " +
                "'Общество' , " +
                "'p1011773' , " +
                "'<img src=\"http://podrobnosti.ua/upload/news/2015/01/18/1011771_1.jpg\" alt=\"\" width=\"58\" height=\"44\" /> Украина вернет Донбасс и возродит там все украинское. Об этом в своем выступлении на Марше памяти жертв террора под Волновахой заявил президент Петр Порошенко. Украина продемонстрировала всему миру, что является миролюбивым европейским государством, отметил президент.' , " +
                "'http://podrobnosti.ua/upload/news/2015/01/18/1011771_1.jpg' , " +
                "'Донбасс вернет Луганск и возродит там все украинское. Об этом в своем выступлении на Марше памяти жертв террора под Волновахой заявил президент Петр Порошенко. Украина продемонстрировала всему миру, что является миролюбивым европейским государством, отметил президент.' , " +
                "'' , " +
                "'http://podrobnosti.ua/upload/news/2015/01/18/1011769_3.jpg' )";
        db.execSQL(sql);

    }

    /**
     * Returns all the customers in the table
     */
    public Cursor getAllTopics(Integer site) {
        SQLiteDatabase db = getReadableDatabase();//do not close it until cursor released
        if (site != null) {
            String sql = SQLiteQueryBuilder.buildQueryString(
                    false, DB_TABLE, new String[]{KEY_ROW_ID, TOPIC_TITLE, TOPIC_LINK, TOPIC_AUTHOR, TOPIC_AGENCY, TOPIC_PUBLICATION_DATE, TOPIC_CATEGORY_DOMAIN, TOPIC_CATEGORY, TOPIC_GUID, TOPIC_DESCRIPTION, TOPIC_DESCRIPTION_IMG, TOPIC_DESCRIPTION_TEXT, TOPIC_CONTENT, TOPIC_MEDIA_CONTENT},
                    TOPIC_SITE + " = ? ", null, null, KEY_ROW_ID + " desc ", null);
            Log.wtf("NewsReader WTF", sql);
            Log.wtf("NewsReader WTF", site.toString());
            return db.query(DB_TABLE, new String[]{KEY_ROW_ID, TOPIC_TITLE, TOPIC_LINK, TOPIC_AUTHOR, TOPIC_AGENCY, TOPIC_PUBLICATION_DATE, TOPIC_CATEGORY_DOMAIN, TOPIC_CATEGORY, TOPIC_GUID, TOPIC_DESCRIPTION, TOPIC_DESCRIPTION_IMG, TOPIC_DESCRIPTION_TEXT, TOPIC_CONTENT, TOPIC_MEDIA_CONTENT},
                    TOPIC_SITE + " = ? ", new String[]{site.toString()}, null, null,
                    KEY_ROW_ID + " desc ");
        } else {
            return db.query(DB_TABLE, new String[]{KEY_ROW_ID, TOPIC_TITLE, TOPIC_LINK, TOPIC_AUTHOR, TOPIC_AGENCY, TOPIC_PUBLICATION_DATE, TOPIC_CATEGORY_DOMAIN, TOPIC_CATEGORY, TOPIC_GUID, TOPIC_DESCRIPTION, TOPIC_DESCRIPTION_IMG, TOPIC_DESCRIPTION_TEXT, TOPIC_CONTENT, TOPIC_MEDIA_CONTENT},
                    null, null, null, null,
                    KEY_ROW_ID + " desc ");
        }
    }

    /**
     * on DB version number Upgrade listener
     *
     * @param arg0
     * @param arg1
     * @param arg2
     */
    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        // my code here
    }
}
