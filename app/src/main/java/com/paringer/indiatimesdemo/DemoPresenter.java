package com.paringer.indiatimesdemo;

import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.bumptech.glide.Glide;
import com.paringer.indiatimesdemo.dao.ContentCacheDAO;
import com.paringer.indiatimesdemo.dao.DbCachedTopicsHelper;
import com.paringer.indiatimesdemo.dao.TopicsDAO;
import com.paringer.indiatimesdemo.dao.TopicsLoader;
import com.tuesda.walker.circlerefresh.CircleRefreshLayout;

import java.io.IOException;

import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Presenter pattern
 * all in one, in one, shiny, bottle
 */
public class DemoPresenter implements CircleRefreshLayout.OnCircleRefreshListener {
    private AppCompatActivity mActivity;
    private TopicsLoader mTopicsLoader;
    private SimpleCursorAdapter mSimpleCursorAdapter;

    private CircleRefreshLayout mRefreshLayout;
    private ListView mList;
    private DbCachedTopicsHelper mDbHelper;

    /**
     * constructor
     *
     * @param activity      activity context
     * @param list          list
     * @param refreshLayout
     */
    public DemoPresenter(AppCompatActivity activity, ListView list, CircleRefreshLayout refreshLayout) {
        mActivity = activity;
        mList = list;
        mRefreshLayout = refreshLayout;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitNetwork().build();
        StrictMode.setThreadPolicy(policy);

        mDbHelper = new DbCachedTopicsHelper(new TopicsDAO(mActivity));
        new TopicsDAO(mActivity).getWritableDatabase().close();
        new ContentCacheDAO(mActivity).getWritableDatabase().close();
//        mDbCachedTopicsHelper = new DbCachedTopicsHelper(activity);
//        mDbCachedUrlHelper = new DbCachedUrlHelper(activity);
        mSimpleCursorAdapter = new SimpleCursorAdapter(mActivity, R.layout.topic_item, null,
                new String[]{TopicsDAO.TOPIC_TITLE,
                        TopicsDAO.TOPIC_DESCRIPTION_IMG,
                        TopicsDAO.TOPIC_DESCRIPTION, TopicsDAO.TOPIC_PUBLICATION_DATE, TopicsDAO.TOPIC_AGENCY, TopicsDAO.TOPIC_AUTHOR},
                new int[]{R.id.textView1title,
                        R.id.imageIconUrl,
                        R.id.textView2text, R.id.textView3date, R.id.textView4Agency, R.id.textView5Author},
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        ) {
            @Override
            public void setViewImage(ImageView v, String value) {
                Glide
                        .with(mActivity)
                        .load(value)
                        .placeholder(android.R.drawable.ic_menu_view)
                        .into(v);
            }
        };
        this.mList.setAdapter(mSimpleCursorAdapter);
        this.mRefreshLayout.setOnRefreshListener(this);

        /** Creating a loader for populating listview from sqlite database */
        /** This statement, invokes the method onCreatedLoader() */
        mTopicsLoader = new TopicsLoader(mActivity, mSimpleCursorAdapter);
        mActivity.getLoaderManager().initLoader(0, null, mTopicsLoader);
//        mSimpleCursorAdapter.notifyDataSetChanged();
//        reloadAndRefresh(mList);
    }

    /**
     * cleanup of all fields before destruction of object
     */
    void cleanup() {
        if (mSimpleCursorAdapter != null) mSimpleCursorAdapter.swapCursor(null);
        if (mActivity != null) mActivity.getLoaderManager().destroyLoader(0);
        if (mList != null) mList.setAdapter(null);
        if (mList != null) mList.setOnItemClickListener(null);

        mActivity = null;
        mTopicsLoader = null;
        mSimpleCursorAdapter = null;
        mRefreshLayout = null;
        mList = null;
        mDbHelper = null;
    }

    /**
     * cleanup of all fields before destruction of object
     *
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        cleanup();
        super.finalize();
    }

    /**
     * refresh and reload list content of news topics list
     */
    @Override
    public void refreshing() {
        // do something when refresh starts

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(Constants.SITE_URL)  //call your base url
//                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        IndiaTimesNewsFeedService indiaTimesService = restAdapter.create(IndiaTimesNewsFeedService.class);
//        Callback<ResponseBody> callback = new Callback<ResponseBody>();
        retrofit2.Callback<IndiaTimesNewsFeedService.NewsPage> callback = new retrofit2.Callback<IndiaTimesNewsFeedService.NewsPage>() {
            @Override
            public void onResponse(retrofit2.Response<IndiaTimesNewsFeedService.NewsPage> response) {
                mRefreshLayout.finishRefreshing();
                Log.d("WTF OK", "" + response.message());
                Log.d("WTF OK", "" + response.code());
                Log.d("WTF OK", "" + response.body());
                try {
                    Log.d("WTF", "" + (response.errorBody() != null ? response.errorBody().string() : "null"));
                } catch (IOException e) {
                }
                for (IndiaTimesNewsFeedService.NewsPage.NewsItem n : response.body().NewsItem) {
                    Log.d("WTF OK", n.HeadLine);
//                    Log.d("WTF OK", n.Keywords);
//                    Log.d("WTF OK", n.ByLine);
//                    Log.d("WTF OK", n.Agency);
//                    Log.d("WTF OK", n.DateLine);
                    if (n.Video != null) {
                        if (n.Video.isJsonObject())
                            Log.d("WTF OK VIDEO SINGLE", n.Video.getAsJsonObject().get("VideoCaption").getAsString());
                        if (n.Video.isJsonArray())
                            Log.d("WTF OK VIDEO ARRAY", n.Video.getAsJsonArray().get(0).getAsJsonObject().get("VideoCaption").getAsString());
                    }
                    Log.d("WTF OK", "\n");
                }
                mDbHelper.insertFeed(response.body(), Integer.valueOf("http://timesofindia.indiatimes.com/feeds/newsdefaultfeeds.cms?feedtype=sjson".hashCode()));
                refreshList();
            }

            @Override
            public void onFailure(Throwable t) {
                mRefreshLayout.finishRefreshing();
                refreshList();
                Log.e("WTF", "" + t);
            }
        };
        retrofit2.Call<IndiaTimesNewsFeedService.NewsPage> feedNews = indiaTimesService.feedNews("sjson"); // ghService.listRepos("paringer", callback);
        feedNews.enqueue(callback);
    }

    /**
     * method of
     *
     * @link CircleRefreshLayout.OnCircleRefreshListener
     */
    public void refreshList() {
        mActivity.getLoaderManager().restartLoader(0, null, mTopicsLoader);//new TopicsLoader(mActivity, mSimpleCursorAdapter)
        mList.deferNotifyDataSetChanged();
        mList.setAdapter(mList.getAdapter());
//        mList.scrollTo(0,0);
        mList.invalidate();
    }

    /**
     * method of
     *
     * @link CircleRefreshLayout.OnCircleRefreshListener
     */
    @Override
    public void completeRefresh() {
        // do something when refresh complete
//        mList.deferNotifyDataSetChanged();
        new Handler(mActivity.getMainLooper())
                .postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                mRefreshLayout.onTouchEvent(MotionEvent.obtain(0, 0, MotionEvent.ACTION_MOVE, 0.0f, 0.0f, 0));
                            }
                        },
                        2555
                );
    }
}
