package com.paringer.indiatimesdemo;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.paringer.indiatimesdemo.dao.ContentCacheDAO;
import com.paringer.indiatimesdemo.dao.TopicsDAO;
import com.paringer.indiatimesdemo.dao.TopicsLoader;
import com.tuesda.walker.circlerefresh.CircleRefreshLayout;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * An activity representing a list of NewsList. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link NewsDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link NewsListFragment} and the item details
 * (if present) is a {@link NewsDetailFragment}.
 * <p/>
 * This activity also implements the required
 * {@link NewsListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class NewsListActivity extends AppCompatActivity
        implements NewsListFragment.Callbacks {
    @Bind(R.id.refresh_layout)
    CircleRefreshLayout mRefreshLayout;
    @Bind(R.id.newsTopicsList)
    ListView mList;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private DemoPresenter mPresenter;

    /**
     * called on creation
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_app_bar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Loading...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                if (mPresenter != null) mPresenter.refreshing();
            }
        });

        if (findViewById(R.id.news_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((NewsListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.news_list_fragment))
                    .setActivateOnItemClick(true);
        }

    }

    /**
     * called on stop
     */
    @Override
    protected void onStop() {
        if (mPresenter != null) mPresenter.cleanup();
        mPresenter = null;
        super.onStop();
    }

    /**
     * called on start
     */
    @Override
    protected void onStart() {
        ButterKnife.bind(this);
        mPresenter = new DemoPresenter(this, mList, mRefreshLayout);
        super.onStart();
    }

    /**
     * called on resume
     */
    @Override
    protected void onResume() {
//        mList.setAdapter(mList.getAdapter());
//        this.getLoaderManager().getLoader(0).onContentChanged();
//        this.getLoaderManager().restartLoader(0,null, new TopicsLoader(this,(SimpleCursorAdapter) mList.getAdapter()));
//        mList.deferNotifyDataSetChanged();
//        mList.invalidate();
        if (mPresenter != null) mPresenter.refreshList();
        super.onResume();
    }

    /**
     * Callback method from {@link NewsListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(ListView listView, View view, int position, long id) {
        if (listView == null) {
            return;
        }
        if (listView.getAdapter() == null) {
            return;
        }
        Cursor cursor = (Cursor) listView.getAdapter().getItem(position);
//        int c = listView.getAdapter().getCount();
        if (cursor == null) {
            return;
        }//down position click
        String link = cursor.getString(cursor.getColumnIndex(TopicsDAO.TOPIC_LINK));
        String guid = cursor.getString(cursor.getColumnIndex(TopicsDAO.TOPIC_GUID));
//        Intent intent = new Intent(this ,ContentActivity.class);
//        intent.putExtra(TopicsDAO.TOPIC_LINK, link);
//        intent.putExtra(TopicsDAO.TOPIC_GUID, guid);
//        startActivityFromFragment(getFragmentManager().findFragmentById(R.id.content_container), intent, 0);

        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(NewsDetailFragment.ARG_ITEM_ID, Long.toString(id));
            arguments.putString(NewsDetailFragment.ARG_TOPIC_LINK, link);
            arguments.putString(NewsDetailFragment.ARG_TOPIC_GUID, guid);
            NewsDetailFragment fragment = new NewsDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.news_detail_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, NewsDetailActivity.class);
            detailIntent.putExtra(NewsDetailFragment.ARG_ITEM_ID, Long.toString(id));
            detailIntent.putExtra(NewsDetailFragment.ARG_TOPIC_LINK, link);
            detailIntent.putExtra(NewsDetailFragment.ARG_TOPIC_GUID, guid);


            startActivity(detailIntent);
        }
    }
}
